output "region" {
  value = var.region
}

output "environment" {
  value = var.environment
}

# output "private_subnet_ids" {
#   value = [aws_subnet.private.*.id]
# }

# output "public_subnet_ids" {
#   value = [aws_subnet.public.*.id]
# }

# output "azs" {
#   value = [var.azs]
# }

output "vpcid" {
  value = aws_vpc.main.id
}

