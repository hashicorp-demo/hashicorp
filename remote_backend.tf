terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "Troy"
    workspaces {
      name = "gitlab-demo"
    }
  }
}