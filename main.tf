module "network" {
  source         = "./network"
  name           = "gitlab"
  environment    = "dev"
  region         = "us-east-1"
  azs            = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
  cidr_block     = "10.10.8.0/21"
  private_ranges = ["10.10.8.0/24", "10.10.9.0/24", "10.10.10.0/24", "10.10.11.0/24"]
  public_ranges  = ["10.10.12.0/24", "10.10.13.0/24", "10.10.14.0/24", "10.10.15.0/24"]
  private_sn     = "true"
  tags = {
    Environment      = module.network.environment
    Application_Name = "GitLab Demo"
    Cost_Center      = "9876"
    Contact          = "Ben"
  }
}

# module "stack" {
#   source             = "./stack"
#   ami                = "ami-02ccb28830b645a41"
#   instance_type      = "t2.micro"
#   environment        = module.network.environment
#   region             = module.network.region
#   azs                = module.network.azs
#   vpcid              = module.network.vpcid
#   private_subnet_ids = module.network.private_subnet_ids
#   public_subnet_ids  = module.network.public_subnet_ids
#   public_key         = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7QD8fTP3gwArd+T2RiUMFzUYiycuM8PQZ4NRsfyu2z1zDFMON0IolBw//ZlT3QNpAHoBYTodvDD6MG/F6ihbfXZoR9JMy5aI/fw0cxUs8NFxC4XSERj52a9fhKbqiFNm2/pA2ezl7Y0dVqzMB6Lchy8ma6mpGK0agJefDH/cKhs4YVjiAuvGuur76KMj1dUz/RmJo/cSbhY7TxdXKsZ8V0ZNPC8Rl44E0LpUjc4xNC0st1YXEOx5MIhW8Z/qpOm7HoyvQHJVusRjCB2QLFk2oMLwC9ngjWbuQuDfyKvj1kzc0AdqdqXOPakQoT9o9cYkXssCYnXQXwntEEXaURxIkQzny45jq34UxsvWIOz74IRasLFOavQIimqJ+xwpMLgSjWDql4xARYZvl48eAvK2QVsS9kXu+D5VOxdrvKYL0WPj0M7gZp0Co82J+KNqZW53/MEMt/YrioxNW5IXajjbmEG79zNzMRiSByGTS7qDJI37iLqklq1C3vnQ0TlLeNlphKtSUcLQPbws5sz8/rnkf18U2bn9wFJvUiePAWjYdTpHjghwZNW6xG0ud01IT07lh46SDcddymLXJV2DLHsM6HgNrLDdO8QZ3PS3gHDW3z38ucguVe1mM7YS7Cg0FuqW17DbatQDD7Aq7QJQYnszVGqzI8MqNwgaJbil1Co0+dw=="
# }

# output "elb_public_name" {
#   value = module.stack.elb_public_name
# }

