provider "aws" {
  region                  = var.region
}

resource "aws_vpc" "main" {
  cidr_block = var.cidr_block
  tags = merge(
    var.tags,
    {
      "Name"        = "${var.name}-${var.environment}-vpc"
    },
  )
}

# Moved SG from stack module for Demo Sentinel rule
resource "aws_security_group" "allow_web_traffic" {
  name        = "allow_web_traffic"
  description = "Allow HTTP inbound web traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.10.10.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    var.tags,
    {
      "Name" = "${var.name}-${var.environment}-sg"
    },
  )
}

resource "aws_subnet" "public" {
  count                   = length(var.azs)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(concat(var.public_ranges, [""]), count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = true

  tags = merge(
    var.tags,
    {
      "Name"        = "${var.name}-${var.environment}-public${count.index + 1}"
    },
  )
}

# resource "aws_subnet" "private" {
#   count             = length(var.azs)
#   vpc_id            = aws_vpc.main.id
#   cidr_block        = element(concat(var.private_ranges, [""]), count.index)
#   availability_zone = element(var.azs, count.index)

#   tags = merge(
#     var.tags,
#     {
#       "Name"        = "${var.name}-${var.environment}-private${count.index + 1}"
#     },
#   )
# }

# resource "aws_internet_gateway" "igw" {
#   vpc_id = aws_vpc.main.id
#   tags = merge(
#     var.tags,
#     {
#       "Name" = "${var.name}-${var.environment}-igw"
#     },
#   )
# }

# resource "aws_route_table" "private" {
#   count  = length(var.azs)
#   vpc_id = aws_vpc.main.id

#   tags = merge(
#     var.tags,
#     {
#       "Name" = "${var.name}-${var.environment}-private-az${count.index + 1}"
#     },
#   )
# }

# resource "aws_route_table" "public" {
#   count  = length(var.azs)
#   vpc_id = aws_vpc.main.id

#   tags = merge(
#     var.tags,
#     {
#       "Name" = "${var.name}-${var.environment}-public-az${count.index + 1}"
#     },
#   )
# }

# resource "aws_route_table_association" "private" {
#   count          = length(aws_subnet.private)
#   subnet_id      = aws_subnet.private[count.index].id
#   route_table_id = aws_route_table.private[count.index >= length(var.azs) ? count.index % length(var.azs) : count.index].id
# }

# resource "aws_route" "internet_access" {
#   route_table_id         = aws_vpc.main.main_route_table_id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.igw.id
# }

# resource "aws_route" "nat_access" {
#   count                  = length(aws_subnet.private)
#   route_table_id         = aws_route_table.private[count.index >= length(var.azs) ? count.index % length(var.azs) : count.index].id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_nat_gateway.nat[count.index >= length(var.azs) ? count.index % length(var.azs) : count.index].id
# }

# resource "aws_nat_gateway" "nat" {
#   count         = length(var.azs)
#   allocation_id = aws_eip.nat_eip[count.index].id
#   subnet_id     = aws_subnet.public[count.index].id

#   tags = merge(
#     var.tags,
#     {
#       "Name" = "${var.name}-${var.environment}-public${count.index + 1}"
#     },
#   )
# }

# resource "aws_eip" "nat_eip" {
#   count = length(var.azs)
#   vpc   = true

#   tags = merge(
#     var.tags,
#     {
#       "Name" = "${var.name}-${var.environment}-eip${count.index + 1}"
#     },
#   )
# }

