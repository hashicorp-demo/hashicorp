variable "name" {
  type        = string
  description = "VPC Name"
}

variable "environment" {
  type        = string
  description = "Environment"
}

variable "region" {
  type        = string
  description = "AWS region"
}

variable "azs" {
  type        = list(string)
  description = "AWS AZs"
}

variable "cidr_block" {
  type        = string
  description = "IP CIDR Block"
}

variable "private_ranges" {
  type        = list(string)
  description = "Private Subnets"
}

variable "public_ranges" {
  type        = list(string)
  description = "Public Subnets"
}

variable "private_sn" {
  type        = string
  description = "Private and public subnets"
  default     = "false"
}

variable "tags" {
  description = "Required tags for the environment"
  type        = map(string)
  # default = {
  #   environment      = "dev"
  #   application_name = "HashiCorp Demo"
  #   cost_center      = "1234"
  #   contact          = "John Doe"
  # }
}

